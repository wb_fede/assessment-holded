export const data = {
    cards: [
        {
            title: 'Ventas',
            subtitle: 'Año Actual',
            content: '274.38€',
            footerTitle: '0% del objetivo',
            footerSubtitle: '0,00€'
        },
        {
            title: 'Gastos',
            subtitle: 'Año Actual',
            content: '0,00€',
            footerTitle: '0% del objetivo',
            footerSubtitle: '0,00€'
        },
        {
            title: 'Beneficio',
            subtitle: 'Año Actual',
            content: '274.38€',
            footerTitle: '0% del objetivo',
            footerSubtitle: '0,00€'
        }
    ]
};
