import { IconDefinition } from '@fortawesome/free-solid-svg-icons';

export interface OptionLink {
    icon?: IconDefinition;
    img?: string;
    navRoute: string;
    linkText?: string;
    label?: string;
}
