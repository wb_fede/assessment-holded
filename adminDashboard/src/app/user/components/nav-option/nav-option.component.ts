import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: '[app-nav-option]',
  templateUrl: './nav-option.component.html',
  styleUrls: ['./nav-option.component.scss']
})
export class NavOptionComponent implements OnInit {
  @Input() icon;
  @Input() navRoute;
  @Input() linkText;
  @Input() label;
  @Input() img;
  @Input() tooltip = 'right';

  @Output() onClick: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() { }

  onClickHandler() {
    console.log('I should be clicking');
    this.onClick.emit(true);
  }

}
