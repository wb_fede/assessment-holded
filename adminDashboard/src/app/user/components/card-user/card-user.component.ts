import { Component, OnInit } from '@angular/core';
import { SIDEBARLINKS } from '../../constants/constants';

@Component({
  selector: 'app-card-user',
  templateUrl: './card-user.component.html',
  styleUrls: ['./card-user.component.scss']
})
export class CardUserComponent implements OnInit {
  links = SIDEBARLINKS;
  constructor() { }

  ngOnInit() {
  }

}
