import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() title;
  @Input() subtitle;
  @Input() content;
  @Input() footerTitle;
  @Input() footerSubtitle;
  @Input() footerColor;
  @Input() icon;
  constructor() { }

  ngOnInit() {
  }

}
