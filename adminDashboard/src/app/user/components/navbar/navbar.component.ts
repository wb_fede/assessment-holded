import { Component, OnInit, Input } from '@angular/core';
import { OptionLink } from '../../models/optionLink';
import { faBars, faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  @Input() links: OptionLink[] = [];
  @Input() actions: OptionLink[] = [];
  
  iconMobile = faBars;

  constructor() { }

  ngOnInit() {
  }

  toggleNavbar(){
    const drawerContainer = document.querySelector('.actionsContainer');
    drawerContainer.classList.toggle('toggleNavbar');
    ( this.iconMobile === faBars) ? this.iconMobile = faTimes : this.iconMobile = faBars;
  }

}
