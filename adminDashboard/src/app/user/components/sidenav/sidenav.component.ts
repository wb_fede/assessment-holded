import { Component, OnInit, Input, HostListener } from '@angular/core';
import { OptionLink } from '../../models/optionLink';
import { faBars, faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SideNavComponent implements OnInit {
  @Input() links: OptionLink[] = [];
  @Input() actions: OptionLink[] = [];
  iconMobile = faBars;
  iconClose = faTimes;

  constructor() {
   }

  ngOnInit() {
  }

  toggleSidenav(){
    const drawerContainer = document.querySelector('.sidenav');
    drawerContainer.classList.toggle('toggleSidebar');
  }

}
