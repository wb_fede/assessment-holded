import { OptionLink } from '../models/optionLink';
import {
  faMagic,
  faHistory,
  faBriefcase,
  faShoppingCart,
  faUserFriends,
  faTag,
  faLandmark,
  faFile,
  faChartBar,
  faChartPie,
  faNewspaper,
  faDoorClosed,
  faSearch,
  faPlusCircle,
  faBell,
  faCog,
  faUser
} from '@fortawesome/free-solid-svg-icons';

export const SIDEBARLINKS: OptionLink[] = [
    {
      icon: faMagic,
      linkText: 'Home',
      navRoute: 'dashboard',
    },
    {
      icon: faHistory,
      linkText: 'History',
      navRoute: 'history',
    },
    {
      icon: faBriefcase,
      linkText: 'Briefcase',
      navRoute: 'briefcase',
    },
    {
      icon: faShoppingCart,
      linkText: 'ShoppingCart',
      navRoute: 'shopping-cart',
    },
    {
      icon: faUserFriends,
      linkText: 'Users',
      navRoute: 'user-friends',
    },
    {
      icon: faTag,
      linkText: 'Tag',
      navRoute: 'tag',
    },
    {
      icon: faFile,
      linkText: 'Files',
      navRoute: 'file',
    },
    {
      icon: faLandmark,
      linkText: 'Landmark',
      navRoute: 'landmark',
    },
    {
      icon: faChartBar,
      linkText: 'Chart',
      navRoute: 'chart-bar',
    },
    {
      icon: faChartPie,
      linkText: 'Chart Pie',
      navRoute: 'chart-pie',
    },
    {
      icon: faNewspaper,
      linkText: 'Newspaper',
      navRoute: 'newspaper',
    },
    {
      icon: faDoorClosed,
      linkText: 'Close',
      navRoute: 'door-closed',
    }
];


export const NAVBARLINKS: OptionLink[] = [
  {
    img: 'facturacion',
    linkText: 'Facturacion',
    navRoute: 'dashboard',
  },
  {
    img: 'contabilidad',
    linkText: 'Contabilidad',
    navRoute: 'dashboard',
  },
  {
    img: 'crm',
    linkText: 'crm',
    navRoute: 'dashboard',
  },
  {
    img: 'equipo',
    linkText: 'equipo',
    navRoute: 'dashboard',
  },
  {
    img: 'proyecto',
    linkText: 'proyecto',
    navRoute: 'dashboard',
  },
  {
    img: 'inventario',
    linkText: 'inventario',
    navRoute: 'dashboard',
  }
];


export const NAVBARACTION: OptionLink[] = [
  {
    icon: faPlusCircle,
    linkText: 'Agregar',
    navRoute: 'add',
  },
  {
    icon: faSearch,
    linkText: 'Buscar',
    navRoute: 'search',
  },
  {
    icon: faBell,
    linkText: 'Notificaciones',
    navRoute: 'notification',
  },
  {
    icon: faCog,
    linkText: 'Ajustes',
    navRoute: 'settings',
  },
  {
    icon: faUser,
    linkText: 'Usuario',
    navRoute: 'user',
  }
];
