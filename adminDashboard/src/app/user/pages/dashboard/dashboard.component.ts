import { Component, OnInit } from '@angular/core';
import { data } from '../../services/data.services';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  data = data;
  constructor() { }

  ngOnInit() {
  }

}
