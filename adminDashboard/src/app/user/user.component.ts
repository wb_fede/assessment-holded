import { OnInit, Component } from '@angular/core';
import { SIDEBARLINKS, NAVBARLINKS, NAVBARACTION } from './constants/constants';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss']
  })
  export class UserComponent implements OnInit {
    sidebarLinks = SIDEBARLINKS;
    nabvarLinks = NAVBARLINKS;
    actionLink = NAVBARACTION;
    constructor() { }

    ngOnInit() {

    }
}
