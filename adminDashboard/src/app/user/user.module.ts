import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';

// Icons
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

// Components
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { NavOptionComponent } from './components/nav-option/nav-option.component';
import { SideNavComponent } from './components/sidenav/sidenav.component';
import { UserComponent } from './user.component';

// boostrap
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LogoComponent } from './components/logo/logo.component';
import { CardComponent } from './components/card/card.component';
import { CardUserComponent } from './components/card-user/card-user.component';
import { SimpleCardComponent } from './components/simple-card/simple-card.component';
import { FloatButtonComponent } from './components/float-button/float-button.component';

@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    FontAwesomeModule,
    NgbTooltipModule
  ],
  declarations: [
    UserComponent,
    DashboardComponent,
    SideNavComponent,
    NavOptionComponent,
    NavbarComponent,
    LogoComponent,
    CardComponent,
    CardUserComponent,
    SimpleCardComponent,
    FloatButtonComponent
  ]
})
export class UserModule { }
