<?php
// get params
if(! isset($_GET['currencies'])){
    echo "Error - The currency is missing. You must pass the param 'currencies'.
    If you want All, use the currency 'ALL'";
    return;
}
$currency = $_GET['currencies'];
// data
$url =  'http://apilayer.net/api/';
$endpoint = 'live';
$access_key = '503c696c67fd2f703f6ddec99902a2ae';

// call api rest
$ch = curl_init($url.$endpoint.'?access_key='.$access_key.'');
// setting curl 
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_TIMEOUT, 5000);

$json = curl_exec($ch);
$exchangeRates = json_decode($json, true);

curl_close($ch);
// error
if(!$exchangeRates['success']){
    echo "An error occurred while processing your request.";
    return;
}

if(strtoupper($currency) == 'ALL'){
    foreach ($exchangeRates['quotes'] as $currency => $value) {
        echo '1 USD : '.$value.' '. str_replace("USD", "", $currency)."<br>";
    } 
    return;
}
// array of currencies
$arrayCurrency = preg_split("/,/",$currency);

foreach ($arrayCurrency as  $curr) {
    $currencyRate = 'USD'.$curr;
    if (!array_key_exists($currencyRate, $exchangeRates['quotes'])) {
        echo "Error - The Currency doesnt exists";
    }else{
        echo '1 USD: '.$exchangeRates['quotes'][$currencyRate].' '.$curr;
    }
    echo "<br>";
}

?>