# Excercise 1 - HTML y CSS

## Getting Started

### Versions

    - Jquery 3.4.1

### Installing

	1 - Clone the repository
    2 - Open Index File using a server or browser directly

## Documentation

## Constants
This file contains all constants for the app, for example, SideNav-NavBar actions or links.

## Dashboard
This screen contains all user's components.

## SCSS
Path: /assets/scss

This folder contains diferents SCSS Partials and Mixins.

## SASS

To generate CSS file, I used SASS to compile SCSS files into it.

SASS Documentation: https://sass-lang.com/

## Authors

Devoto Federico 

# Excercise 5 - Rest API

## Getting Started

### Versions

	- PHP7
    - Jquery 3.3.1

### Installing

	1 - Download Files
    2 - PHP File: You need a server to execute this file, for example, Apache
    3 - Jquery File: Open this file on browser

## Documentation

### PHP
Path: Localhost/FileName-path?currencies={{Currencies values (One or more with ,)}}
Parameters: 
    - Currencies (Mandatory): This can be only 1 value or an array separated by comma
    - Currencies values allowed: https://currencylayer.com/documentation
Method: GET
Response Type: Text-HTML

#### Responses

    - Missing Paramenter
    Error - The currency is missing. You must pass the param 'currencies'. If you want All, use the currency 'ALL'

    - Incorrect Currency
    Error - The Currency doesnt exists

    - Service Error
    An error occurred while processing your request.

#### Example

Request: http://localhost/AssessmentHolded/RestApi/getCurrencyRates.php?currencies=ARS
Response: 1 USD: 45.017497 ARS

Request: http://localhost/AssessmentHolded/RestApi/getCurrencyRates.php?currencies=ARS,EUR
Response: 
1 USD: 45.017497 ARS
1 USD: 0.897265 EUR



### Jquery
Path: FilePath?currencies={{Currencies values (One or more with ,)}}
Parameters: 
    - Currencies (Mandatory): This can be only 1 value or an array separated by comma
    - Currencies values allowed: https://currencylayer.com/documentation
Method: GET
Response Type: Text-HTML

#### Responses

    - Missing Paramenter
    Error - The currency is missing. You must pass the param 'currencies'. If you want All, use the currency 'ALL'

    - Incorrect Currency
    Error - The Currency doesnt exists

    - Service Error
    An error occurred while processing your request.

#### Example

Request: file:///C:/xampp/htdocs/AssessmentHolded/RestApi/getCurrencyRates.html?currencies=ARS
Response: 1 USD: 45.017497 ARS

Request: file:///C:/xampp/htdocs/AssessmentHolded/RestApi/getCurrencyRates.html?currencies=ARS,EUR
Response: 
1 USD: 45.017497 ARS
1 USD: 0.897265 EUR

## Authors

Devoto Federico 

