const SIDEBARLINKS = [{
        icon: 'magic',
        linkText: 'Home',
        navRoute: 'dashboard',
    },
    {
        icon: 'history',
        linkText: 'History',
        navRoute: 'history',
    },
    {
        icon: 'briefcase',
        linkText: 'Briefcase',
        navRoute: 'briefcase',
    },
    {
        icon: 'shopping-cart',
        linkText: 'ShoppingCart',
        navRoute: 'shopping-cart',
    },
    {
        icon: 'user-friends',
        linkText: 'Users',
        navRoute: 'user-friends',
    },
    {
        icon: 'tag',
        linkText: 'Tag',
        navRoute: 'tag',
    },
    {
        icon: 'file',
        linkText: 'Files',
        navRoute: 'file',
    },
    {
        icon: 'landmark',
        linkText: 'Landmark',
        navRoute: 'landmark',
    },
    {
        icon: 'chart-bar',
        linkText: 'Chart',
        navRoute: 'chart-bar',
    },
    {
        icon: 'chart-pie',
        linkText: 'Chart Pie',
        navRoute: 'chart-pie',
    },
    {
        icon: 'newspaper',
        linkText: 'Newspaper',
        navRoute: 'newspaper',
    },
    {
        icon: 'door-closed',
        linkText: 'Close',
        navRoute: 'door-closed',
    }
];


const NAVBARLINKS = [{
        img: 'facturacion',
        linkText: 'facturacion',
        navRoute: 'dashboard',
    },
    {
        img: 'contabilidad',
        linkText: 'Contabilidad',
        navRoute: 'dashboard',
    },
    {
        img: 'crm',
        linkText: 'crm',
        navRoute: 'dashboard',
    },
    {
        img: 'equipo',
        linkText: 'equipo',
        navRoute: 'dashboard',
    },
    {
        img: 'proyecto',
        linkText: 'proyecto',
        navRoute: 'dashboard',
    },
    {
        img: 'inventario',
        linkText: 'inventario',
        navRoute: 'dashboard',
    }
];


const NAVBARACTION = [{
        icon: 'plus-circle',
        linkText: 'Agregar',
        navRoute: 'add',
    },
    {
        icon: 'search',
        linkText: 'Buscar',
        navRoute: 'search',
    },
    {
        icon: 'bell',
        linkText: 'Notificaciones',
        navRoute: 'notification',
    },
    {
        icon: 'cog',
        linkText: 'Ajustes',
        navRoute: 'settings',
    },
    {
        icon: 'user',
        linkText: 'Usuario',
        navRoute: 'user',
    }
];

const data = {
    cards: [{
            title: 'Ventas',
            subtitle: 'Año Actual',
            content: '274.38€',
            footerTitle: '0% del objetivo',
            footerSubtitle: '0,00€'
        },
        {
            title: 'Gastos',
            subtitle: 'Año Actual',
            content: '0,00€',
            footerTitle: '0% del objetivo',
            footerSubtitle: '0,00€'
        },
        {
            title: 'Beneficio',
            subtitle: 'Año Actual',
            content: '274.38€',
            footerTitle: '0% del objetivo',
            footerSubtitle: '0,00€'
        }
    ],
    simpleCard: [{
            title: 'Ventas',
            subtitle: 'Año Actual',
            footer: '274.38€'
        },
        {
            title: 'Gastos',
            subtitle: 'Año Actual',
            footer: '0,00€'
        },
        {
            title: 'Beneficio',
            subtitle: 'Año Actual',
            footer: '274.38€'
        },
        {
            title: 'Beneficio',
            subtitle: 'Año Actual',
            footer: '274.38€'
        }
    ],
};