$(function() {
    // Init links and options
    // Navbar links
    NAVBARLINKS.forEach(function(link) {
        $('.navbar .linkItem').append(`
        <a class="nav-option" data-toggle="tooltip" data-placement="bottom" title="${link.linkText}">
            <img src="assets/img/${link.img}.png" alt="${link.linkText}">
        </a>
        `);
    });

    // Navbar Actions
    NAVBARACTION.forEach(function(link) {
        $('.navbar .actionItem').append(`
        <li>
            <a class="nav-option" data-toggle="tooltip" data-placement="bottom" title="${link.linkText}">
            <i class="fas fa-${link.icon}"></i>
                <p class="option-text">
                ${link.linkText}
                </p>
            </a>
        </li>
        `);
    });

    // Sidebar Links
    SIDEBARLINKS.forEach(function(link) {
        $('.sidenav .links').append(`
        <a class="nav-option" data-toggle="tooltip" data-placement="right" title="${link.linkText}">
        <i class="fas fa-${link.icon}"></i>
            <p class="option-text">
                ${link.linkText}
            </p>
        </a>
        `);
    });

    // cards loop
    data.cards.forEach(function(card, i) {
        $('.grid').append(`
            <div class="card" style="grid-area: card${i}">
                <div class="card-header">
                    <h4>${card.title}</h4>
                    <h6>${card.subtitle}</h6>
                </div>
                <div class="card-body">
                    body
                </div>
                <div class="card-footer">
                    <h6>${card.footerTitle}</h6>
                    <span>${card.footerSubtitle}</span>
                </div>
            </div>
        `);
    });

    // add simple card
    data.simpleCard.forEach(function(card, i) {
        $('.grid').append(`
            <div class="card simpleCard gridItem simplecard${i}">
                <h4>${card.title}</h4>
                <h5>${card.subtitle}</h5>
                <h6>${card.footer}</h6>
            </div>
        `);
    });

    // user card
    SIDEBARLINKS.forEach(function(link) {
        $('.grid .linkItem').append(`
        <a class="nav-option" title="${link.linkText}">
        <i class="fas fa-${link.icon}"></i>
            <p class="option-text">
            ${link.linkText}
            </p>
        </a>
        `);
    });
    // enable Tooltip
    if ($(window).width() > 960) {
        $('[data-toggle="tooltip"]').tooltip();
    }


    // toggle mobile menu
    $(".navbar .mobileMenu").click(function() {
        navbarToggle();
    });

    // toggle sidebar menu mobile
    $(".sidenav .mobileMenu").click(function() {
        sidebartoggle();
    });

    // toggle button clicked sidenav and nabvar
    $(".navbar .nav-option").click(function() {
        navbarToggle();
    });
    $(".sidenav .nav-option").click(function() {
        sidebartoggle();
    });


    // Functions
    function navbarToggle() {
        $('.actionsContainer').toggleClass('toggleNavbar');
        $('.navbar .mobileMenu i').toggleClass('fas fa-bars fas fa-times');
    }

    function sidebartoggle() {
        $('.sidenav').toggleClass('toggleSidebar');
        $('.sidenav .mobileMenu i').toggleClass('fas fa-bars fas fa-times');
    }

});